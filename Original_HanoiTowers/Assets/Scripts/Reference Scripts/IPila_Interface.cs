using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPila_Interface
{
    void InicializarPila(int c);

    int Apilar(int x);

    int Desapilar();

    int Tope();

    bool PilaVacia();

    void ImpresiónPila();
}
