using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilaOne : MonoBehaviour, IPila_Interface
{   
    /*Lo primero que necesito es un array que me guarde
     * la cantidad de datos que voy a usar
     */
    int[] datos;

    /*En segundo lugar necesito un entero que almacene
     * la cantidad de datos m�ximos ya que el array no es din�mico
     */
    int cantidadMaximaDatos;

    /*Y finalmente necesito el �ndice que va a ir llevando
     * cuenta del �ltimo elemento que entra en la pila*/
    int indice;

    public void InicializarPila(int cantidad)
    {
        /*En el m�todo de Inicializaci�n lo que hago es igualar todo a 0
         * para poder inicializarlo. Recibimos un entero llamado cantidad 
         * y lo igualamos a la cantidad m�xima de datos
         * lo mismo con los datos y el �ndice*/
        cantidadMaximaDatos = cantidad;
        datos = new int[cantidad];
        indice = 0;
    }

    public int Apilar(int x)
    {
        /*ac� hay que corroborar que justamente el �ndice, o sea el dato que
         * voy a ir buscar est� en una posici�n MENOR a la de la cantidad m�xima de datos
         * EN ESE CASO, se suma uno al �ndice*/
        if (indice < cantidadMaximaDatos)
        {
            x = datos[indice];
            indice++;
            return indice;
        }
        else
            return 0;
    }

    public int Desapilar()
    {
        /*despu�s, por el contrario, le sacamos uno al �ndice
         * si la pila NO est� vac�a*/
        if (!PilaVacia())
        {
            indice--;
            return indice;
        }
        else
            return 0;
    }

    public int Tope()
    {
        /*ac� vamos a buscar el �ltimo dato que queremos
         * que est� en el array de datos, vamos al �ndice y le sacamos 1*/
        return datos[indice - 1];
    }

    public bool PilaVacia()
    {
        return indice == 0;
    }

    public void Impresi�nPila()
    {
        Debug.Log("imprimir la pikla");
    }
}
