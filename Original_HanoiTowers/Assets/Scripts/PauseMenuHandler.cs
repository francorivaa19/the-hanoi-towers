using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuHandler : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuScreen = null;

    [SerializeField] private Button resumeButton = null;
    [SerializeField] private Button exitButton = null;


    void Update()
    {
        if (Input.GetKey(KeyCode.L) || Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("cual");
            Time.timeScale = 0f;
            pauseMenuScreen.SetActive(true);
        }
    }

    private void ResumeGame()
    {
        pauseMenuScreen.SetActive(false);
    }

    private void QuitGame()
    {
        Application.Quit();
    }
}
