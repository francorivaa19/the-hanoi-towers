using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPila_Hanoi 
{
    void Inicializar(int amount);

    void Apilar(GameObject block);

    GameObject Desapilar();

    GameObject Tope();

    bool PilaVacia();

    public int GetIndex();
}
