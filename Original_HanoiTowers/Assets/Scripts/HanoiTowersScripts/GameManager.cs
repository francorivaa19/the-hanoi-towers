using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Towers")]
    [SerializeField] private FirstTower pila1;
    [SerializeField] private FirstTower pila2;
    [SerializeField] private FirstTower pila3;

    [Header("Pieces")]
    [SerializeField] private GameObject piece1;
    [SerializeField] private GameObject piece2;
    [SerializeField] private GameObject piece3;
    [SerializeField] private GameObject piece4;

    private void Start()
    {
        pila1.Inicializar(4);

        pila1.Apilar(piece1);
        pila1.Apilar(piece2);
        pila1.Apilar(piece3);
        pila1.Apilar(piece4);
    }
}
