using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTower : MonoBehaviour, IPila_Hanoi
{
    public GameObject[] blocks;
    public int index;

    public void Inicializar(int amount)
    {
        index = 0;
    }

    public void Apilar(GameObject block)
    {
        blocks[index] = block;
        index++;      
    }

    public GameObject Desapilar()
    {
        if (!PilaVacia())
        {
            var aux = blocks[index - 1];
            blocks[index - 1] = null;
            index--;
            return aux;
        }

        else return null;
             
    }

    public GameObject[] GetStack()
    {
        return blocks;
    }

    public GameObject Tope()
    {
        if (!PilaVacia())
        {
            return blocks[index - 1];
        }
        else return null;
        
    }

    public GameObject TopBlock()
    {
        return blocks[index - 1];
    }

    public bool PilaVacia()
    {
        return index == 0;
    }

    public int GetIndex()
    {
        return index - 1;
    }
}
