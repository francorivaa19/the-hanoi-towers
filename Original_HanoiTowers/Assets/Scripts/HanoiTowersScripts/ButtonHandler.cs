using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour
{
    #region towers and buttons

    [Header("Pilas")]
    [SerializeField] private FirstTower pila1;
    [SerializeField] private FirstTower pila2;
    [SerializeField] private FirstTower pila3;

    [Header("Buttons")]
    [SerializeField] private Button leftButton;
    [SerializeField] private Button doubleLeftButton;
    [SerializeField] private Button middleButtonToLEFT;
    [SerializeField] private Button middleButtonToRIGHT;
    [SerializeField] private Button rightButton;
    [SerializeField] private Button doubleRightButton;

    #endregion towers and buttons

    #region sound settings

    [Header("SoundSettings")]
    private bool canPlaySound = false;
    [SerializeField] private AudioClip clickSound = null;
    [SerializeField] private AudioClip errorSound = null;
    private AudioSource audioSource;

    #endregion sound settings

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        leftButton.onClick.AddListener(LeftClickHandler);
        doubleLeftButton.onClick.AddListener(DoubleLeftButtonHandler);
        middleButtonToLEFT.onClick.AddListener(MiddleButtonToLeftHandler);
        middleButtonToRIGHT.onClick.AddListener(MiddleButtonToRightHandler);
        rightButton.onClick.AddListener(RightButtonHandler);
        doubleRightButton.onClick.AddListener(DoubleRightButtonHandler);
    }

    private void Start()
    {
        canPlaySound = true;
    }

    #region click handlers

    public void LeftClickHandler()
    {
        MovePiece(pila1, pila2, 5f);

        if (canPlaySound)
        {
            audioSource.clip = clickSound;
            audioSource.Play();
        }
        else
        {
            audioSource.clip = errorSound;
            audioSource.Play();
        }
    }

    public void DoubleLeftButtonHandler()
    {
        MovePiece(pila1, pila3, 10f);

        if (canPlaySound) PlayClickSound();
        else PlayErrorSound();
    }

    public void MiddleButtonToLeftHandler()
    {
        MovePiece(pila2, pila1, -5f);

        if (canPlaySound) PlayClickSound();
        else PlayErrorSound();
    }

    public void MiddleButtonToRightHandler()
    {
        MovePiece(pila2, pila3, 5f);

        if (canPlaySound) PlayClickSound();
        else PlayErrorSound();
    }

    public void RightButtonHandler()
    {
        MovePiece(pila3, pila2, -5f);

        if (canPlaySound) PlayClickSound();
        else PlayErrorSound();
    }

    public void DoubleRightButtonHandler()
    {
        MovePiece(pila3, pila1, -10);

        if (canPlaySound) PlayClickSound();
        else PlayErrorSound();
    }

    #endregion click handlers

    public void MovePiece(FirstTower torreInicial, FirstTower torreFinal, float moveAmount)
    {
        var topeTorreInicial = torreInicial.Tope().GetComponent<Transform>().localScale.x;
        var topeTorreFinal = torreFinal.Tope();
        float torreFinalY = topeTorreFinal != null ? topeTorreFinal.GetComponent<Transform>().localScale.x : 0f;

        if (topeTorreInicial < torreFinalY || topeTorreFinal == null)
        {
            canPlaySound = true;

            var obj = torreInicial.Desapilar();
            Transform transform = obj.GetComponent<Transform>();
            var movement = transform.position.x + moveAmount;
            transform.position = new Vector3(movement, torreFinal.GetIndex() - 1, 0f);
            torreFinal.Apilar(obj);
        }

        else canPlaySound = false;
    }

    private void PlayClickSound()
    {
        audioSource.clip = clickSound;
        audioSource.Play();
    }

    private void PlayErrorSound()
    {
        audioSource.clip = errorSound;
        audioSource.Play();
    }
}
