using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuHandler : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button exitButton;

    [SerializeField] private GameObject mainMenuScreen = null;

    [SerializeField] private AudioClip clickSound;
    private AudioSource audioSrc;

    private void Awake()
    {
        audioSrc = GetComponent<AudioSource>();

        playButton.onClick.AddListener(StartGame);
        exitButton.onClick.AddListener(ExitGame);
    }

    public void StartGame()
    {
        audioSrc.clip = clickSound;
        audioSrc.Play();

        mainMenuScreen.SetActive(false);
    }

    public void ExitGame()
    {
        audioSrc.clip = clickSound;
        audioSrc.Play();

        Application.Quit();
    }
}
